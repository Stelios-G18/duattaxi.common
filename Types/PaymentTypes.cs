﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DuaTaxi.Common.Types
{
    public enum  PaymentTypes
    {
        Free =0,

        PayPal =1,

        Iban =2,

        Card =3,

        Taxi_PayPal_10 = 4,

        Taxi_PayPal_27 = 5,

        Taxi_PayPal_50 = 6,
        
    }
}
