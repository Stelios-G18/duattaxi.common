﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DuaTaxi.Common.Types
{
    public enum DriverPositionStatus
    {
        NewRide = 0,
        OnPassengerWay = 1,
        FromPassengerToDestination = 2,
        RideFinished = 3

    }
}
