﻿using DuaTaxi.Common.CustomCheck;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace DuaTaxi.Common.Authentication
{

    public class AuthValidation : ActionFilterAttribute, IAuthorizationFilter
    {
        private readonly CheckOptions _check;


        public AuthValidation(CheckOptions check)
        {
            _check = check;
        }


        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context != null) {             

                var _token = context.HttpContext.Request.Headers[HeaderNames.Authorization];
                var tmp = _token.ToString();
                
                if (_token.ToString() != "" ) {
                    string authToken = _token;
                    if (authToken != null) {
                        if (AuthValidated(_token)) {
                            context.HttpContext.Response.Headers.Add("authToken", authToken);
                            context.HttpContext.Response.Headers.Add("AuthStatus", "Authorized");

                            context.HttpContext.Response.Headers.Add("storeAccessiblity", "Authorized");

                            return;
                        }
                        else {
                            context.HttpContext.Response.Headers.Add("authToken", authToken);
                            context.HttpContext.Response.Headers.Add("AuthStatus", "NotAuthorized");

                            context.HttpContext.Response.StatusCode = (int)HttpStatusCode.Forbidden;
                            context.HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "Not Authorized";
                            context.Result = new JsonResult("NotAuthorized") {
                                Value = new {
                                    Status = "Error",                                 
                                },
                            };
                        }

                    }

                }
                else {
                    context.HttpContext.Response.StatusCode = (int)HttpStatusCode.ExpectationFailed;
                    context.HttpContext.Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = "Please Provide authToken";
                    context.Result = new JsonResult("Please Provide authToken") {
                        Value = new {
                            Status = "Error",                             
                        },
                    };
                }
            }
        }

        private bool AuthValidated(string token)
        {

            if (token.Substring(7) != _check.Password) {
                return false;
            }
            return true;
        }



    }

}
