﻿using Autofac;
using DuaTaxi.Common.CustomCheck;
using Microsoft.Extensions.Configuration;


namespace DuaTaxi.Common.CustomApiCheck
{
    public static class Extensions
    {
        public static void AddCustomerCheck(this ContainerBuilder builder)
        {
          
            builder.Register(context => {
                var configuration = context.Resolve<IConfiguration>();
                var options = configuration.GetOptions<CheckOptions>("check");

                return options;
            }).SingleInstance();
        }
    }
}
